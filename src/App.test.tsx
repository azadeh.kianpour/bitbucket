import { Post } from './reducers/postReducer';
import { AxiosResponse } from 'axios';
import posts from './api';

const expected = [
  "id",
  "title",
  "content",
  "lat",
  "long",
  "image_url",
  "created_at",
  "updated_at"
];

test('test respoanse schema', async () => {
  const response: AxiosResponse<Post[]> = await posts.get('/post/1');
  const { data } = response;
  expected.forEach(property => {
    expect(data).toHaveProperty(property);
  });
});

