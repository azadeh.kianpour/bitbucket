import ReactMapboxGl from 'react-mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import './index.css';

const mapBoxAPIKey = process.env.REACT_APP_MAP_BOX_API_KEY;

interface IMapPropos {
    center?: [number, number]
    children?: any;
}

function Map(props: IMapPropos) {
    if (!mapBoxAPIKey) {
        return <span>No API key assigned to MapBox</span>
    }

    const Mapbox = ReactMapboxGl({
        accessToken: mapBoxAPIKey ? mapBoxAPIKey : ''
    });
    const style: any = {
        height: '100%',
        width: '100%'
    };

    return (
        <Mapbox
            zoom={[3]}
            center={props.center}
            style={"mapbox://styles/mapbox/streets-v9"}
            containerStyle={style}>
            {props.children}
        </Mapbox>
    )
}

export default Map;