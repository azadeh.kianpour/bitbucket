import React from "react";
import { Marker } from 'react-mapbox-gl';

export interface IMapMarkerProps {
    lat: number;
    long: number;
}

export class MapMarker extends React.Component<IMapMarkerProps, any> {
    render() {
        const { lat, long } = this.props;

        return (
            <Marker coordinates={[long, lat]}>
                <span className="map-marker-container">
                    <span className='map-marker-image' />
                </span>
            </Marker>
        )
    }
}