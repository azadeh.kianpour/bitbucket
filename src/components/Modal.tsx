import { ReactPortal } from 'react';
import ReactDOM from 'react-dom';

export const Modal = (props: any): ReactPortal => {
    return ReactDOM.createPortal(
        <div
            onClick={props.onDismiss}
            className="post-delete-container"
        >
            <div onClick={e => e.stopPropagation()}>
                <div className="header">{props.title}</div>
                <div className="content">{props.content}</div>
                <div className="actions">{props.actions}</div>
            </div>
        </div>,
        document.querySelector('#modal') as HTMLElement
    );
};
