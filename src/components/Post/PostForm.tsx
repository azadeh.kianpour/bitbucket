import React, { useState } from 'react';
import { Post } from '../../reducers/postReducer';
import { Form, Input, Button, Alert } from 'antd';
import { Link } from 'react-router-dom';
import Map from '../Map';
import { MapMarker } from '../Map/MapMarker';

const { TextArea } = Input;

export interface FormValues {
    title: string;
    content: string;
    lat: number;
    long: number;
    image_url?: string;
}

export type OwnPostFormProps = {
    onSubmit: (post: Post) => void;
    post?: Post;
};

export const PostForm: React.FunctionComponent<OwnPostFormProps> = props => {
    const { post } = props;

    const [lat, setLat] = useState(post?.lat)
    const [long, setLong] = useState(post?.long);
    const [error, setError] = useState('');

    let titleElement: any, contentElement: any, latElement: any, longElement: any, image_urlElement: any;

    const layout = {
        labelCol: { span: 8 },
        wrapperCol: { span: 16 },
    };

    const submitForm = () => {
        let formData: Post;

        const title = titleElement.input.value;
        const content = contentElement.resizableTextArea.props.value;
        const lat = latElement.input.value;
        const long = longElement.input.value;
        const image_url = image_urlElement.input.value;

        if (!title) {
            setError('Title is empty.')
            return;
        }

        if (!content) {
            setError('Content is empty.')
            return;
        }

        if (!post) { /*CREATE*/
            formData = {
                id: 0,
                title,
                content,
                lat,
                long,
                image_url,
                created_at: (new Date()).toISOString()
            }
        } else {/*EDIT*/
            formData = {
                id: post.id,
                title,
                content,
                lat,
                long,
                image_url,
                updated_at: (new Date()).toISOString()
            }
        }

        props.onSubmit(formData);
        window.history.back(); //TODO: user route
    }

    return (
        <>
            {error ? <Alert message={error} type="error" /> : <></>}

            <div className='post-container'>
                <div className='post-form'>
                    <Form
                        {...layout}
                        name="edit-post"
                    >
                        <Form.Item
                            label="Title"
                            name="title"
                            rules={[{ required: true, message: 'Please input title!' }]}>
                            <Input ref={(el) => titleElement = el} defaultValue={post?.title} />
                        </Form.Item>

                        <Form.Item
                            label="Content"
                            name="content"
                            rules={[{ required: true, message: 'Please input content!' }]}>
                            <TextArea rows={4} ref={(el) => contentElement = el} defaultValue={post?.content} />
                        </Form.Item>

                        <Form.Item
                            label="Image"
                            name="image">
                            <Input ref={(el) => image_urlElement = el} defaultValue={post?.image_url} />
                        </Form.Item>

                        <Form.Item
                            label="Lat"
                            name="lat">
                            <Input ref={(el) => latElement = el} defaultValue={post?.lat} onBlur={(e: any) => setLat(e.target.value)} style={{ width: '100px' }} />
                        </Form.Item>

                        <Form.Item
                            label="Long"
                            name="long">
                            <Input ref={(el) => longElement = el} defaultValue={post?.long} onBlur={(e: any) => setLong(e.target.value)} style={{ width: '100px' }} />
                        </Form.Item>
                    </Form>
                </div>
                <span className='map-container'>
                    <Map center={lat && long ? [long, lat] : undefined}>
                        {lat && long && <MapMarker lat={lat} long={long} />}
                    </Map>
                </span>
                <span className='button-container'>
                    <Button type="primary" onClick={submitForm}>Submit</Button>
                    <Link to='/'>
                        <Button>Cancel</Button>
                    </Link>
                </span>
            </div>
        </>
    )
};
