import React, { Component } from 'react';
import { PostForm } from '../../components/Post/PostForm';
import { connect } from 'react-redux';
import { addPost } from '../../actions/postActions';
import { Post } from '../../reducers/postReducer';

export type OwnPostsNewProps = {
    addPost: (post: Post) => void;
};

class PostsNew extends Component<OwnPostsNewProps> {
    render() {
        if (!this.props.addPost) {
            return null;
        }
        return (
            <PostForm
                onSubmit={this.props.addPost}
            />
        );
    }
}

export default connect(
    null,
    { addPost }
)(PostsNew);
