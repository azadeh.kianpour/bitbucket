import React from 'react';
import { Modal } from '../../components/Modal';
import { connect } from 'react-redux';
import { RootState } from '../../store';
import { RouteComponentProps } from 'react-router';
import { fetchPost, deletePost } from '../../actions/postActions';
import { Post } from '../../reducers/postReducer';
import { Link } from 'react-router-dom';
import history from '../../history';
import { Button } from 'antd';
import { Route } from 'react-router-dom'

interface PostDeleteProps extends RouteComponentProps<OwnPropsParams> {
    post: Post;
    fetchPost: (id: number) => void;
    deletePost: (post: number) => void;
}

class PostDelete extends React.Component<PostDeleteProps> {
    componentDidMount(): void {
        this.props.fetchPost(Number(this.props.match.params.id));
    }

    renderActions() {
        const { id } = this.props.match.params;
        return (
            <div className='button-container'>
                <Route render={({ history }) => (
                    <Button type="primary"
                        onClick={() => { this.props.deletePost(Number(id)); history.push('/posts') }}
                    >
                        Delete
                    </Button>
                )} />
                <Link to="/">
                    <Button>
                        Cancel
                    </Button>
                </Link>
            </div>
        );
    }

    render() {
        const { post } = this.props;
        if (!post) return <></>

        const message = <>
            Post with title <strong>"{post.title}"</strong> will be deleted.
            <p>Are you sure?</p>
        </>

        return (
            <Modal
                title={message}
                actions={this.renderActions()}
                onDismiss={() => history.push('/')}
            />
        );
    }
}

interface OwnPropsParams {
    id: string;
}

function mapStateToProps(
    state: RootState,
    ownProps: RouteComponentProps<OwnPropsParams>
) {
    return {
        post: state.posts.items[Number(ownProps.match.params.id)]
    };
}

export default connect(
    mapStateToProps,
    { fetchPost, deletePost }
)(PostDelete);
