import { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { fetchPosts } from '../../actions/postActions';
import { RootState } from '../../store';
import { Post } from '../../reducers/postReducer';
import { Card, Image, PageHeader, Button } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import './Post.css';
import Map from '../Map';
import { MapMarker } from '../Map/MapMarker';

const { Meta } = Card;

export interface PostsListProps {
  posts: Post[];
  fetchPosts: () => any;
}

class PostsList extends Component<PostsListProps> {
  componentDidMount(): void {
    this.props.fetchPosts();
  }

  render() {
    const { posts } = this.props;
    if (!posts) {
      return null;
    }

    const fallBackSrc = process.env.REACT_APP_IMAGE_FALL_BACK_SRC;
    return <>
      <PageHeader>
        <Link to='/posts/new'>
          <Button>Create</Button>
        </Link>
      </PageHeader>

      <div className='posts'>
        {
          posts.map(post => {
            return <Card
              key={post.id}
              hoverable
              style={{ width: 360, margin: 24 }}
              actions={[
                <Link to={'/posts/edit/' + post.id}>
                  <EditOutlined key="edit" title="edit" />
                </Link>,
                <Link to={'/posts/delete/' + post.id}>
                  <DeleteOutlined key="delete" title="delete" />
                </Link>
              ]}
              cover={
                <Image
                  width='100%'
                  src={post.image_url}
                  fallback={fallBackSrc}
                  placeholder={
                    <Image
                      preview={false}
                      src={post.image_url}
                      fallback={fallBackSrc}
                      width={200}
                    />
                  }
                />
              }>
              <Meta title={post.title} description={post.content} />
              {post.lat && post.long &&
                <span className='map-container' style={{ display: 'block' }}>
                  <Map center={[post.long, post.lat]}>
                    <MapMarker lat={post.lat} long={post.long} />
                  </Map>
                </span>}
            </Card>
          })
        }
      </div></>
  }
}

const mapStateToProps = (state: RootState) => {
  return {
    posts: _.values(state.posts.items)
  };
};

export default connect(
  mapStateToProps,
  { fetchPosts }
)(PostsList);
