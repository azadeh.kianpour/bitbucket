import { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
import "./App.css";

import PostList from "./components/Post/PostList";
import PostNew from "./components/Post/PostNew";
import PostEdit from "./components/Post/PostEdit";
import PostDelete from "./components/Post/PostDelete";
import { Layout } from 'antd';

const { Header, Content } = Layout;

class App extends Component {
  render() {
    return (
      <Router>
        <Layout>
          <Header style={{ backgroundColor: '#501E96', color: '#fff', fontSize: '16pt' }}>
            <Link to='/'>
              Bitbucket
            </Link>
          </Header>
          <Content>
            <Switch>
              <Route path="/" exact component={PostList} />
              <Route path="/posts" exact component={PostList} />
              <Route path="/posts/new" exact component={PostNew} />
              <Route
                path="/posts/edit/:id"
                exact
                component={PostEdit}
              />
              <Route
                path="/posts/delete/:id"
                exact
                component={PostDelete}
              />
            </Switch>
          </Content>
        </Layout>
      </Router>
    );
  }
}

export default App;